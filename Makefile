#
# MPI for LuaJIT.
# Copyright © 2013 Peter Colberg.
# For conditions of distribution and use, see copyright notice in LICENSE.
#

mpi:
	@$(MAKE) -C mpi

clean:
	@$(MAKE) -C mpi clean
	@$(MAKE) -C doc clean

.PHONY: mpi clean
