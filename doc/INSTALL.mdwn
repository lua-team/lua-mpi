% Installing

MPI for LuaJIT is available from a [git repository]:

~~~
git clone http://git.colberg.org/luajit-mpi
~~~

[git repository]: http://git.colberg.org/luajit-mpi

Before using the module, generate matching C bindings for the MPI environment:

~~~
make
~~~

To override the default MPI C compiler `mpicc`, set `CPP`:

~~~
make CPP=mpicc.mpich
~~~

To override the name of the MPI library, set `LIBNAME`:

~~~
make LIBNAME=libmpich.so.10
~~~
